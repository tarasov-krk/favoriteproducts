<?php namespace Itcom\FavoriteProducts\Components;

use Input;
use Cms\Classes\ComponentBase;
use itcom\favoriteproducts\classes\FavoriteProductsService;

/**
 * Class FavoriteProducts
 *
 * @package Itcom\FavoriteProducts\Components
 */
class FavoriteProducts extends ComponentBase
{
    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'Избранные товары',
            'description' => '',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [];
    }

    /**
     * @return void
     */
    public function onRun()
    {
        $this->page['favoriteProducts'] = $this->getProductsId();
    }

    /**
     * @return array|string
     */
    public function getProductsId()
    {
        return FavoriteProductsService::getAllItems();
    }

    /**
     * Add product to favorite
     *
     * @return boolean
     */
    public function onAdd()
    {
        if ($productId = Input::get('product_id')) {
            FavoriteProductsService::AddItem($productId);
        }

        return true;
    }

    /**
     * Remove product from favorite
     *
     * @return boolean
     */
    public function onRemove()
    {
        if ($productId = Input::get('product_id')) {
            FavoriteProductsService::RemoveItem($productId);
        }

        return true;
    }

    /**
     * Clear favorite products
     *
     * @return boolean
     */
    public function onClear()
    {
        FavoriteProductsService::ClearItems();

        return true;
    }

    /**
     *
     * @return boolean
     */
    public function onAjax()
    {
        return true;
    }
}
