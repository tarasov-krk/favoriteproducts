<?php namespace Itcom\Favoriteproducts;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Itcom\Favoriteproducts\Components\FavoriteProducts' => 'FavoriteProducts',
        ];
    }

    public function registerSettings()
    {
    }
}
