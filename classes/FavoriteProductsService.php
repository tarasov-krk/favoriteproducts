<?php namespace itcom\favoriteproducts\classes;

use Illuminate\Support\Facades\Cookie;

class FavoriteProductsService
{
    const COOKIE_NAME = 'itcom_favorite_products';
    const COOKIE_LIFE_TIME = 2419200;

    private static $cacheProducts = null;

    public static function getAllItems()
    {
        // Получить список добавленных продуктов из куки
        $products = Cookie::get(self::COOKIE_NAME);

        if (!is_null(self::$cacheProducts)) {
            $products = self::$cacheProducts;
        }

        if ($products) {
            $products = explode(',', $products);
        }

        return $products;
    }

    public static function AddItem($id)
    {
        // Получить список добавленных продуктов из куки
        $products = Cookie::get(self::COOKIE_NAME);

        // Добавить продукт в список
        if ($products) {
            $products = explode(',', $products);
        }
        else {
            $products = [];
        }

        array_push($products, $id);
        $products = implode(",", $products);

        // Записать в куки
        Cookie::queue(self::COOKIE_NAME, $products, self::COOKIE_LIFE_TIME);
    }

    public static function RemoveItem($id)
    {
        // Получить список добавленных продуктов из куки
        $products = Cookie::get(self::COOKIE_NAME);

        // Добавить продукт в список
        if ($products) {
            $products = explode(',', $products);
            if (in_array($id, $products)) {
                foreach ($products as $key => $item) {
                    if ($item == $id) {
                        unset($products[$key]);
                    }
                }
            }

            $products = implode(',', $products);
        }

        self::$cacheProducts = $products;

        \Log::error('REmove');

        // Записать в куки
        Cookie::queue(self::COOKIE_NAME, $products, self::COOKIE_LIFE_TIME);
    }

    public static function ClearItems()
    {
        // Записать в куки
        Cookie::queue(self::COOKIE_NAME, '', self::COOKIE_LIFE_TIME);
    }
}
